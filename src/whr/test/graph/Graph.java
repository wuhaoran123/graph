package whr.test.graph;


import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

//图结构类
public class Graph {

    public static final boolean ISDIRE = false;

    public static final boolean NODIRE = true;

    public static boolean NowStatus = true;

    public int V;

    private int E;

    private Bag<Edge>[] adj;

    private LinkedList<Edge> allEdge = new LinkedList<>();

    private ArrayList<Node<Person>> nodes;

    private Map<String, List<Node>> groupByClass;

    public Graph(ArrayList<Node<Person>> nodes) {

        if (nodes.isEmpty()) {
            throw new IllegalArgumentException("请勿输入空的节点集合");
        }


        this.groupByClass = nodes.stream().collect(groupingBy(Node::getItemName));

        this.V = nodes.size();
        this.nodes = nodes;
        this.E = 0;

        adj = (Bag<Edge>[]) new Bag[V];

        for (int v = 0; v < V; v++) {
            adj[v] = new Bag<>();
        }


    }

    public Graph(LinkedList<Edge> edges, ArrayList<Node<Person>> nodes,boolean isDire) {

        this(nodes);

        if (edges.isEmpty()) {
            throw new IllegalArgumentException("请勿输入空的Edge集合");
        }

        if (isDire) {

            List<Edge> otherEdge = edges.stream().map(a -> {
                if(a.getRelative()==null){
                    return new Edge(a.getSecond(), a.getFirst(), a.weight());
                }else{
                    return new Edge(a.getSecond(), a.getFirst(), a.weight(),a.getRelative());
                }
            }).collect(Collectors.toList());

            edges.addAll(otherEdge);
        }

        for (Edge edge : edges) {
            addEdge(edge);
            allEdge.add(edge);
        }
    }

    private void addEdge(Edge edge) {

        Node v = edge.getFirst(), w = edge.getSecond();

        adj[v.id].add(edge);
        adj[w.id].add(edge);
    }

    public Iterable<Edge> adj(int index) {

        return adj[index];
    }

    public Iterator<Edge> edges() {

        return allEdge.iterator();
    }

    public Node<Person> getNodes(int index) {

        return nodes.get(index);
    }

    public List<Node<Person>> getNodesByClassAndAge(int age, Class clazz) {

        List<Node<Person>> result = nodes.parallelStream().filter(a -> {

            Person p = a.t;

            if (clazz.equals(p.getClass())) {
                Student s = (Student) a.t;
                return s.getAge() == age;
            } else {
                return false;
            }


        }).collect(Collectors.toList());

        System.out.println("年龄为" + age + "的学生共有 : " + result);

        return result;
    }

    public List<Node> getItemByClass(Class clazz) {

        List<Node> reslute = groupByClass.get(clazz.getName());

        System.out.println("顶点中包含类型为" + clazz.getName() + "共有" + reslute.size() + "个，对应元素名称为" + " : " + reslute);

        return reslute;
    }
}
