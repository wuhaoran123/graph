package whr.test.graph;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

//程序启动入口类
public class Luncher {

    private PathAlgorithm[] all;

    public Luncher(Graph graph) {
        all = new PathAlgorithm[graph.V];
        for (int v = 0; v < graph.V; v++) {
            all[v] = new PathAlgorithm(graph, graph.getNodes(v));
        }
    }

    public Iterable<Edge> path(Node start, Node end) {

        List<Edge> path = all[start.id].pathTo(end);
        LinkedList<String> pathStrList = new LinkedList<>();

        double distance = 0;

        for (Edge edge : path) {

            String stage = edge.getFirst().t.getName() + "->" + edge.getSecond().t.getName();

            distance += edge.weight();

            pathStrList.addFirst(stage);
        }


        if (pathStrList.size() != 0) {
            System.out.println("路径的先后顺序为 ： " + pathStrList);
            System.out.println("路径距离为 : " + distance);
        } else {
            System.out.println("没有可达路径");
        }


        return path;
    }

    private double getDistance(Node start, List<Edge> path, double distance, Edge edge) {

        if (edge.getFirst().equals(start) && path.size() != 1) {
            distance += edge.weight();
        }
        return distance;
    }

    public double dist(Node start, Node end) {
        return all[start.id].distTo(end);
    }

    //测试数据启动
    public static void main(String[] args) {

        ArrayList<Node<Person>> nodes = new ArrayList<>();

        //顶点的构建
        Node node0 = new Node<>(new Student("a", 20));
        Node node1 = new Node<>(new Student("b", 20));
        Node node2 = new Node<>(new Student("c", 30));
        Node node3 = new Node<>(new Teacher("ta", 32));
        Node node4 = new Node<>(new Teacher("tb", 32));
        Node node5 = new Node<>(new Teacher("tc", 33));

        nodes.add(node0);
        nodes.add(node1);
        nodes.add(node2);
        nodes.add(node3);
        nodes.add(node4);
        nodes.add(node5);

        //带权值的边的初始化
        Edge edge1 = new Edge(node0, node2, 1, new ClassMate());
        Edge edge2 = new Edge(node0, node1, 2, new ClassMate());
        Edge edge5 = new Edge(node2, node1, 3, new ClassMate());
        Edge edge6 = new Edge(node2, node3, 3, new TeacherAndStudent());
        Edge edge7 = new Edge(node2, node4, 2, new TeacherAndStudent());
        Edge edge3 = new Edge(node3, node5, 4, new Colleague());
        Edge edge4 = new Edge(node3, node4, 2, new Colleague());


        //不带权值的边的初始化
//        Edge edge1 = new Edge(node0, node2,new ClassMate());
//        Edge edge2 = new Edge(node0, node1,new ClassMate());
//        Edge edge3 = new Edge(node0, node5,new TeacherAndStudent());
//        Edge edge4 = new Edge(node3, node5,new Colleague());
//        Edge edge5 = new Edge(node3, node4,new Colleague());
//        Edge edge6 = new Edge(node2, node1,new ClassMate());
//        Edge edge7 = new Edge(node2, node3,new TeacherAndStudent());
//        Edge edge8 = new Edge(node2, node4,new TeacherAndStudent());

        LinkedList<Edge> edges = new LinkedList<>();
        edges.add(edge1);
        edges.add(edge2);
        edges.add(edge3);
        edges.add(edge4);
        edges.add(edge5);
        edges.add(edge6);
        edges.add(edge7);

        //查询最短路径,无向图
        Graph g = new Graph(edges, nodes,Graph.NODIRE);
        //查询最短路径有相图
        //Graph g = new Graph(edges, nodes,Graph.ISDIRE);
        new Luncher(g).path(node5, node0);

        //查询顶点为Student
        g.getItemByClass(Student.class);

        //获取年龄为20类型为学生的
        g.getNodesByClassAndAge(20, Student.class);

    }

}
