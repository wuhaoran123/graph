package whr.test.graph;
//顶点类
public class Teacher extends Person{

    public String name;

    public int age;

    public Teacher(String name, int age) {
        this.name = name;
        this.age = age;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAge() {
        return 0;
    }
}
