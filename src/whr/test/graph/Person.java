package whr.test.graph;

public abstract class Person implements Cloneable {

    public abstract String getName();

    public abstract int getAge();
}
