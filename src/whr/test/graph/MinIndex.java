package whr.test.graph;

import java.util.Iterator;
import java.util.NoSuchElementException;
//索引数组
public class MinIndex<Key extends Comparable<Key>> implements Iterable<Integer> {

    private int maxN;

    private int N;

    private int[] pq;

    private int[] qp;

    private Key[] keys;

    public MinIndex(int capacity) {
        if (capacity <= 0)
            throw new IllegalArgumentException();
        maxN = capacity;
        N = 0;
        pq = new int[capacity + 1];
        qp = new int[capacity + 1];
        keys = (Key[]) new Comparable[capacity + 1];
        //初始每个索引都没用过
        for (int i = 0; i <= maxN; i++)
            qp[i] = -1;
    }

    public boolean isEmpty() {
        return N == 0;
    }

    public int size() {
        return N;
    }

    public boolean contains(int i) {
        return qp[i] != -1;
    }

    public int minIndex() {
        if (isEmpty())
            throw new NoSuchElementException();
        return pq[1];
    }

    public Key minKey() {
        if (isEmpty())
            throw new NoSuchElementException();
        return keys[pq[1]];
    }

    public Key keyOf(int i) {
        if (!contains(i))
            throw new NoSuchElementException();
        return keys[i];
    }

    public void insert(int i, Key key) {
        if (i < 0 || i >= maxN)
            throw new IllegalArgumentException();
        if (contains(i))
            throw new IllegalArgumentException();
        N++;
        qp[i] = N;
        pq[N] = i;//pq,qp互为映射
        keys[i] = key;
        adjustUp(N);
    }

    public int delMin() {
        if (isEmpty())
            throw new NoSuchElementException();
        int min = minIndex();
        delete(min);
        return min;
    }

    public void delete(int i) {
        if (!contains(i))
            throw new NoSuchElementException();
        int pqi = qp[i];
        swap(pqi, N--);
        adjustUp(pqi);
        adjustDown(pqi);
        qp[i] = -1;
        keys[i] = null;
        pq[N + 1] = -1;
    }

    public void changeKey(int i, Key key) {
        if (!contains(i))
            throw new NoSuchElementException();
        if (keys[i].compareTo(key) == 0)
            throw new IllegalArgumentException();
        if (key.compareTo(keys[i]) > 0)
            increaseKey(i, key);//原键值增加
        else
            decreaseKey(i, key);//原键值减小
    }

    public void decreaseKey(int i, Key key) {
        if (!contains(i))
            throw new NoSuchElementException();
        if (key.compareTo(keys[i]) > 0)
            throw new IllegalArgumentException();
        keys[i] = key;
        int pqi = qp[i];
        adjustUp(pqi);
        adjustDown(pqi);
    }

    public void increaseKey(int i, Key key) {
        if (!contains(i))
            throw new NoSuchElementException();
        if (key.compareTo(keys[i]) < 0)
            throw new IllegalArgumentException();
        keys[i] = key;
        int pqi = qp[i];
        adjustUp(pqi);
        adjustDown(pqi);
    }

    private void swap(int i, int j) {
        int t = pq[i];
        pq[i] = pq[j];
        pq[j] = t;
        int qpi = pq[i];
        int qpj = pq[j];
        qp[qpi] = i;
        qp[qpj] = j;
    }

    private boolean less(int i, int j) {
        int ki = pq[i];
        int kj = pq[j];
        return keys[ki].compareTo(keys[kj]) < 0;
    }

    private void adjustDown(int i) {
        while (2 * i <= N) {
            int l = 2 * i;
            while (l < N && less(l + 1, l))
                l++;
            swap(l, i);
            i = l;
        }
    }

    private void adjustUp(int i) {
        while (i > 1) {
            int p = i / 2;
            if (less(p, i))
                break;
            swap(p, i);
            i = p;
        }
    }


    @Override
    public Iterator<Integer> iterator() {
        return new HeapIterator();
    }

    private class HeapIterator implements Iterator<Integer> {

        MinIndex<Key> copy;

        public HeapIterator() {
            copy = new MinIndex<Key>(maxN);
            for (int i = 1; i <= N; i++) {
                int ki = pq[i];
                Key key = keys[ki];
                copy.insert(ki, key);
            }
        }

        @Override
        public boolean hasNext() {
            return !copy.isEmpty();
        }

        @Override
        public Integer next() {
            if (!hasNext())
                throw new NoSuchElementException();
            return copy.delMin();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}