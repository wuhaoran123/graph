package whr.test.graph;
//定点类
public class Student extends Person{
    public int age;
    public String name;

    public Student(String name, int age) {
        this.age = age;
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getAge() {
        return age;
    }
}
