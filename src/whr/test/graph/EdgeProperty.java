package whr.test.graph;
//边属性包装类
public class EdgeProperty<T extends Relative> {

    private T t;

    public EdgeProperty(T t) {
        this.t = t;
    }

    @Override
    public String toString() {
        return t.relativeStr();
    }
}
