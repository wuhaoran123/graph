package whr.test.graph;

import java.io.Serializable;

//节点类
public  class Node<T extends Person> implements Cloneable,Serializable{
    static int index = 0;
    int id;
    T t;
    public Node(T t) {
        this.t = t;
        id = index;
        index++;
    }

    public Person getItem(){
        return t;
    }

    public String getItemName() {
        return t.getClass().getName();
    }

    @Override
    public String toString() {
        return t.getName();
    }
}
