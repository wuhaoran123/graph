package whr.test.graph;

import java.io.Serializable;

//边结构类
public class Edge implements Comparable<Edge> ,Cloneable,Serializable{

    private Node<Person> first;

    private Node<Person> second;

    private double weight = 1.0;

    private Relative relative;

    public Edge(Node first, Node second, double weight) {
        this.first = first;
        this.second = second;
        this.weight = weight;
    }

    public double weight() {
        return weight;
    }


    public Edge(Node first, Node second, double weight, Relative relative) {
        this.first = first;
        this.second = second;
        this.weight = weight;
        this.relative = relative;
    }

    public Edge(Node first, Node second, Relative relative) {
        this.first = first;
        this.second = second;
        this.relative = relative;
    }


    public Node either() {
        return first;
    }

    public Node other(Node vertex) {
        if (vertex.equals(first)) return second;
        else if (vertex.equals(second)) return first;
        else throw new RuntimeException("边的对应关系不一致");
    }

    @Override
    public int compareTo(Edge that) {
        if (this.weight() < that.weight()) return -1;
        else if (this.weight() > that.weight()) return 1;
        else return 0;
    }

    @Override
    public String toString() {
        return first.t.getName() + "->" + second.t.getName() + " " + weight;
    }

    public Node getFirst() {
        return first;
    }

    public Node getSecond() {
        return second;
    }

    public Edge exchangeNode(){

        Node<Person> temp = this.first;

        this.first = this.second;

        this.second = temp;

        return  this;
    }

    public Relative getRelative() {
        return relative;
    }
}
