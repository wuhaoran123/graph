package whr.test.graph;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;
//背包类
public class Bag<I> implements Iterable {

    ArrayList<I> items = new ArrayList<>();

    public Bag() {

    }

    public void add(I i) {
        items.add(i);
    }

    public boolean isempty() {
        return items.isEmpty();
    }

    public int getSize(){
        return items.size();
    }

    @Override
    public Iterator iterator() {
        return items.iterator();
    }

    @Override
    public void forEach(Consumer action) {
            items.forEach(action);
    }

    @Override
    public Spliterator spliterator() {
        return items.spliterator();
    }
}
