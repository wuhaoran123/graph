package whr.test.graph;


import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Stack;

//最短路径计算类
public class PathAlgorithm {

    private Edge[] edgeTo;

    private double[] distTo;

    private MinIndex<Double> pq;

    public PathAlgorithm(Graph graph, Node start) {


        edgeTo = new Edge[graph.V];

        distTo = new double[graph.V];

        pq = new MinIndex<>(graph.V);

        for (int v = 0; v < graph.V; v++)
            distTo[v] = Double.POSITIVE_INFINITY;
        distTo[start.id] = 0.0;

        pq.insert(start.id, 0.0);

        while (!pq.isEmpty()) {
            relax(graph, graph.getNodes(pq.delMin()));
        }
    }

    private void relax(Edge e) {
        Node w = e.getFirst(), v = e.getSecond();
        if (distTo[w.id] > distTo[v.id] + e.weight()) {
            distTo[w.id] = distTo[v.id] + e.weight();
            edgeTo[w.id] = e;
        }
    }

    private void relax(Graph graph, Node v) {
        for (Edge edge : graph.adj(v.id)) {
            Node w = edge.getSecond();
            if (distTo[w.id] > distTo[v.id] + edge.weight()) {

                distTo[w.id] = distTo[v.id] = edge.weight();
                edgeTo[w.id] = edge;

                if (pq.contains(w.id)) {
                    pq.changeKey(w.id, distTo[w.id]);
                } else {
                    pq.insert(w.id, distTo[w.id]);
                }
            }
        }
    }

    public double distTo(Node node) {
        return distTo[node.id];
    }

    public boolean hasPathTo(Node node) {

        if (!Graph.NowStatus) {
            return true;
        }

        return distTo[node.id] < Double.POSITIVE_INFINITY;
    }

    public List<Edge> pathTo(Node v) {
        if (!hasPathTo(v)) return Collections.emptyList();
        Stack<Edge> path = new Stack<>();
        for (Edge e = edgeTo[v.id]; e != null; e = edgeTo[e.getFirst().id]) {
            path.push(e);
        }

        LinkedList<Edge> copyPath = new LinkedList<>();

        while (!path.isEmpty()) {
            copyPath.addFirst(path.pop());
        }

        return copyPath;
    }
}
